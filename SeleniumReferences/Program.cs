﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;

namespace SeleniumReferences
{
    class Program
    {
        //Create the reference for our browser
        
        static void Main(string[] args)
        {


        }
        [NUnit.Framework.SetUp]
        public void Initialize()
        {
            PropertiesCollection.Driver = new ChromeDriver();
            //navigate to the page
            PropertiesCollection.Driver.Navigate().GoToUrl("http://executeautomation.com/demosite/index.html?UserName=&Password=&Login=Login");
        }

        [NUnit.Framework.Test]
        public void ExecuteTest()
        {
            // Set Title MR.
            SeleniumSetMethod.SelectDropDown("TitleId", "Mr.", PropertyType.Id);
            // Set Initial
            SeleniumSetMethod.EnterText("Initial", "gohar", PropertyType.Name);
            // Get texts from Initial and Title 
            Console.WriteLine(SeleniumGetMethod.GetText("Initial", PropertyType.Name));
            Console.WriteLine(SeleniumGetMethod.GetTextFromDdl("TitleId", PropertyType.Id));
        }

        [NUnit.Framework.TearDown]
        public void CleanUp()
        {
            // driver.Close();
            Console.WriteLine("test completed");

        }
    }
}

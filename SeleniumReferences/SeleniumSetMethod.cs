﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumReferences
{
    class SeleniumSetMethod
    {
        // EnterText(element,value,typeOfControl)
        public static void EnterText(string element, string value, PropertyType typeOfControl)
        {
            if (typeOfControl == PropertyType.Id)
                PropertiesCollection.Driver.FindElement(By.Id(element)).SendKeys(value);
            if (typeOfControl == PropertyType.Name)
                PropertiesCollection.Driver.FindElement(By.Name(element)).SendKeys(value);
        }
        // Perform Click on Sign in
        public static void Click(string element, PropertyType typeOfControl)
        {
            if (typeOfControl == PropertyType.Id)
                PropertiesCollection.Driver.FindElement(By.Id(element)).Click();
            if (typeOfControl == PropertyType.Name)
                PropertiesCollection.Driver.FindElement(By.Name(element)).Click();
           }
        // Select drop-down, check-box
        public static void SelectDropDown(string element, string value, PropertyType typeOfControl)
        {
            if (typeOfControl == PropertyType.Id)

                new SelectElement(PropertiesCollection.Driver.FindElement(By.Id(element))).SelectByText(value);
            if (typeOfControl == PropertyType.Name)
                new SelectElement(PropertiesCollection.Driver.FindElement(By.Name(element))).SelectByText(value);
        }
    }
}
